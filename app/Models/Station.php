<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Station extends Model
{
    use HasFactory, Uuid;

    protected $fillable = ['name', 'address', 'company_id', 'latitude', 'longitude'];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }
}
