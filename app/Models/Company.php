<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    use HasFactory, Uuid;

    protected $fillable = ['name', 'parent_company_id'];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(static::class, 'parent_company_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(static::class, 'parent_company_id')->orderBy('name');
    }

    public function stations(): HasMany
    {
        return $this->hasMany(Station::class);
    }
}
