<?php

namespace App\Http\Requests;

class UpdateCompanyRequest extends StoreCompanyRequest
{
    public function rules(): array
    {
        return array_merge(
            parent::rules(), [
                'name' => 'unique:companies|string|max:128',
            ]
        );
    }
}
