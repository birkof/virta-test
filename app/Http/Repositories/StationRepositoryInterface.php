<?php

namespace App\Http\Repositories;

interface StationRepositoryInterface
{
    public function search(float $latitude, float $longitude, int $radius = 5);
}
