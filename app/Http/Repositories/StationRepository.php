<?php

namespace App\Http\Repositories;

use App\Models\Station;

class StationRepository implements StationRepositoryInterface
{
    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $radius in km
     * @param int|null $companyId
     * @param int $perPage
     * @return mixed
     */
    public function search(
        float $latitude,
        float $longitude,
        int $radius = 5,
        ?int $companyId = null,
        int $perPage = 15
    ): mixed {
        $dbBuilder = Station::selectRaw(
            "id, uuid, name, address, latitude, longitude, company_id, created_at, updated_at,
            ST_Distance_Sphere(
                    Point(?, ?),
                    Point(longitude, latitude)
                ) * ? as distance",
            [$longitude, $latitude, 1000]
        )
            ->whereRaw(
                "ST_Distance_Sphere(
                    Point(?, ?),
                    Point(longitude, latitude)
                 ) < ?",
                [$longitude, $latitude, $radius * 1000]
            );

        if ($companyId) {
            $dbBuilder->whereRaw("company_id = ?", $companyId);
        }

        return $dbBuilder->paginate($perPage);
    }
}
