<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return CompanyResource::collection(Company::paginate($request->page_size ?? 25));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = Company::create($request->validated());

        return CompanyResource::make($company);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $uuid)
    {
        return CompanyResource::make(Company::whereUuid($uuid)->firstOrFail());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCompanyRequest $request, string $uuid)
    {
        $company = Company::whereUuid($uuid)->firstOrFail();

        $company->update($request->validated());

        return CompanyResource::make($company);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $uuid)
    {
        $company = Company::whereUuid($uuid)->firstOrFail()->delete();

        return response()->noContent();
    }
}
