<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStationRequest;
use App\Http\Requests\UpdateStationRequest;
use App\Http\Resources\StationResource;
use App\Models\Station;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return StationResource::collection(Station::paginate($request->page_size ?? 25));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreStationRequest $request)
    {
        $station = Station::create($request->validated());

        return StationResource::make($station);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $uuid)
    {
        return StationResource::make(Station::whereUuid($uuid)->firstOrFail());
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateStationRequest $request, string $uuid)
    {
        $station = Station::whereUuid($uuid)->firstOrFail();

        $station->update($request->validated());

        return StationResource::make($station);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $uuid)
    {
        Station::whereUuid($uuid)->firstOrFail()->delete();

        return response()->noContent();
    }
}
