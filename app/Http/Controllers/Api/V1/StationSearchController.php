<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Repositories\StationRepositoryInterface;
use App\Http\Requests\SearchStationRequest;
use App\Http\Resources\StationResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StationSearchController extends Controller
{
    public function __construct(private readonly StationRepositoryInterface $stationRepository)
    {
    }

    /**
     * Handle the incoming request.
     */
    public function __invoke(SearchStationRequest $request): AnonymousResourceCollection
    {
        return StationResource::collection(
            $this->stationRepository->search(
                $request->validated('latitude'),
                $request->validated('longitude'),
                $request->validated('radius'),
                $request->validated('company_id'),
                $request->page_size ?? 25
            )
        );
    }
}
