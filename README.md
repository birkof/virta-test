


# Virta
## Charging station management system

## Start

You should spin up the entire environment with the following command:

`./vendor/bin/sail up -d`

## Libraries

First of all you need to install vendor libraries by typing:

`./vendor/bin/sail composer install -o`

## Data setup
For having database created & populated type the following:

`./vendor/bin/sail artisan migrate`

`./vendor/bin/sail artisan db:seed --class=CompanySeeder`

`./vendor/bin/sail artisan db:seed --class=StationSeeder`

## Tests

Atm the code is 80.3% covered on tests:

`./vendor/bin/sail artisan test --parallel --coverage`

## Extra 
Open app with:

`./vendor/bin/sail open`

For API testing you can use attached POSTMAN collection

`./VIRTA.postman_collection.json`
