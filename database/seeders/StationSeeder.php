<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Station;
use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Station::factory(50)->create([
            'company_id' => Company::all()->random()->id,
        ])->each(
            function ($parent, $p) {
                Station::factory(rand(1, 5))->create([
                    'company_id' => Company::all()->random()->id,
                ]);
            }
        );
    }
}
