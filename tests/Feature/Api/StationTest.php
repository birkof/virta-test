<?php

namespace Tests\Feature\Api;

use App\Models\Company;
use App\Models\Station;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class StationTest extends TestCase
{
    use RefreshDatabase;

    public function test_listing_stations(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);
        $station = Station::create([
            'name'       => $this->faker->name,
            'address'    => $this->faker->address,
            'company_id' => $company->id,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ]);

        $response = $this->get('/api/v1/stations');
        $response
            ->assertOk()
            ->assertJsonCount(1, 'data')
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'uuid',
                            'name',
                            'address',
                            'company',
                            'latitude',
                            'longitude',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid])
        ;
    }

    public function test_create_new_station(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);

        $payload = [
            'name'       => $this->faker->name,
            'company_id' => $company->id,
            'address'    => $this->faker->address,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ];

        $response = $this->post('/api/v1/stations', $payload);
        $response
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'address',
                        'company',
                        'latitude',
                        'longitude',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid])
        ;

        $this->assertDatabaseHas('stations', $payload);
    }

    public function test_station_details(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);

        $station = Station::create(
            [
                'name'       => $this->faker->name,
                'company_id' => $company->id,
                'address'    => $this->faker->address,
                'latitude'   => $this->faker->latitude,
                'longitude'  => $this->faker->longitude,
            ]
        );

        $response = $this->get(sprintf('/api/v1/stations/%s', $station->uuid));
        $response
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'address',
                        'company',
                        'latitude',
                        'longitude',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid, 'name' => $station->name]);
    }

    public function test_delete_station(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);

        $stationPayload = [
            'name'       => $this->faker->name,
            'company_id' => $company->id,
            'address'    => $this->faker->address,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ];
        $station = Station::create($stationPayload);

        $this->delete(sprintf('/api/v1/stations/%s', $station->uuid))->assertNoContent();

        $this->assertDatabaseMissing('stations', $stationPayload);
    }

    public function test_update_station(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);

        $station = Station::create([
            'name'       => $this->faker->name,
            'company_id' => $company->id,
            'address'    => $this->faker->address,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ]);

        $stationNewName = $this->faker->name;

        $this->put(sprintf('/api/v1/stations/%s', $station->uuid), ['name' => $stationNewName])
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'address',
                        'company',
                        'latitude',
                        'longitude',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid, 'name' => $stationNewName])
        ;
    }

    public function test_update_station_with_invalid_name(): void
    {
        $company = Company::create(['name' => $this->faker->name,]);

        $station = Station::create([
            'name'       => $this->faker->name,
            'company_id' => $company->id,
            'address'    => $this->faker->address,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ]);

        $this->put(sprintf('/api/v1/stations/%s', $station->uuid), ['name' => null])
            ->assertStatus(Response::HTTP_FOUND)
            ->assertInvalid(['name'])
        ;
    }

    public function test_update_station_company(): void
    {
        $company1 = Company::create(['name' => $this->faker->name,]);
        $company2 = Company::create(['name' => $this->faker->name,]);

        $station = Station::create([
            'name'       => $this->faker->name,
            'company_id' => $company1->id,
            'address'    => $this->faker->address,
            'latitude'   => $this->faker->latitude,
            'longitude'  => $this->faker->longitude,
        ]);

        $this->put(sprintf('/api/v1/stations/%s', $station->uuid), ['company_id' => $company2->id])
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'address',
                        'company',
                        'latitude',
                        'longitude',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company2->uuid])
        ;
    }
}
