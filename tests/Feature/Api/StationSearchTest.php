<?php

namespace Tests\Feature\Api;

use App\Models\Company;
use App\Models\Station;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StationSearchTest extends TestCase
{
    use RefreshDatabase;

    public function test_station_search(): void
    {
        $company = Company::create(['name' => $this->faker->name]);

        Station::create(
            [
                'name'       => $this->faker->name,
                'company_id' => $company->id,
                'address'    => $this->faker->address,
                'latitude'   => 44.4363690,
                'longitude'  => 26.1011550,
            ]
        );
        Station::create(
            [
                'name'       => $this->faker->name,
                'company_id' => $company->id,
                'address'    => $this->faker->address,
                'latitude'   => 44.4688490,
                'longitude'  => 26.0875080,
            ]
        );

        $queryParams = [
            'latitude'  => 44.4366050,
            'longitude' => 26.0993170,
            'radius'    => 5,
        ];

        $response = $this->get(sprintf('/api/v1/stations/search?%s', http_build_query($queryParams)));

        $response
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'uuid',
                            'name',
                            'address',
                            'company',
                            'latitude',
                            'longitude',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ]
            )
            ->assertJsonCount(2, 'data')
        ;
    }

    public function test_station_search_for_company(): void
    {
        $company1 = Company::create(['name' => $this->faker->name]);
        $company2 = Company::create(['name' => $this->faker->name]);

        Station::create(
            [
                'name'       => $this->faker->name,
                'company_id' => $company1->id,
                'address'    => $this->faker->address,
                'latitude'   => 44.4363690,
                'longitude'  => 26.1011550,
            ]
        );
        Station::create(
            [
                'name'       => $this->faker->name,
                'company_id' => $company2->id,
                'address'    => $this->faker->address,
                'latitude'   => 44.4688490,
                'longitude'  => 26.0875080,
            ]
        );

        $queryParams = [
            'latitude'   => 44.4366050,
            'longitude'  => 26.0993170,
            'radius'     => 5,
            'company_id' => $company1->id,
        ];

        $response = $this->get(sprintf('/api/v1/stations/search?%s', http_build_query($queryParams)));

        $response
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'uuid',
                            'name',
                            'address',
                            'company',
                            'latitude',
                            'longitude',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ]
            )
            ->assertJsonCount(1, 'data')
        ;
    }
}
