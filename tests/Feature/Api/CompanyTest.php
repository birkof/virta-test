<?php

namespace Tests\Feature\Api;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    public function test_listing_companies(): void
    {
        $companyPayload = ['name' => $this->faker->name,];

        Company::create($companyPayload);

        $this->get('/api/v1/companies')
            ->assertOk()
            ->assertJsonCount(1, 'data')
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'uuid',
                            'name',
                            'parent_company_id',
                            'children',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ]
            );
    }

    public function test_listing_companies_with_children(): void
    {
        $parentCompany = Company::create(['name' => $this->faker->name,]);
        $childCompany= Company::create(['name' => $this->faker->name,'parent_company_id' => $parentCompany->id]);

        $this->get('/api/v1/companies')
            ->assertOk()
            ->assertJsonCount(2, 'data')
            ->assertJsonStructure(
                [
                    'data' => [
                        '*' => [
                            'uuid',
                            'name',
                            'parent_company_id',
                            'children',
                            'created_at',
                            'updated_at',
                        ],
                    ],
                ]
            )
            ->assertJsonFragment(['parent_company_id' => $parentCompany->id])
            ->assertJsonFragment(['uuid' => $childCompany->uuid]);
        ;
    }

    public function test_create_new_company(): void
    {
        $payload = [
            'name' => $this->faker->name,
        ];

        $response = $this->post('/api/v1/companies', $payload);
        $response
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'parent_company_id',
                        'children',
                        'created_at',
                        'updated_at',
                    ],
                ]
            );

        $this->assertDatabaseHas('companies', $payload);
    }

    public function test_company_details(): void
    {
        $company = Company::create(
            [
                'name' => $this->faker->name,
            ]
        );

        $response = $this->get(sprintf('/api/v1/companies/%s', $company->uuid));
        $response
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'parent_company_id',
                        'children',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid, 'name' => $company->name]);
    }

    public function test_delete_company(): void
    {
        $companyPayload = ['name' => $this->faker->name,];
        $company = Company::create($companyPayload);

        $this->delete(sprintf('/api/v1/companies/%s', $company->uuid))->assertNoContent();

        $this->assertDatabaseMissing('companies', $companyPayload);
    }

    public function test_update_company(): void
    {
        $company = Company::create(['name' => $this->faker->name]);

        $companyNewName = $this->faker->name;

        $this->put(sprintf('/api/v1/companies/%s', $company->uuid), ['name' => $companyNewName])
            ->assertOk()
            ->assertJsonStructure(
                [
                    'data' => [
                        'uuid',
                        'name',
                        'parent_company_id',
                        'children',
                        'created_at',
                        'updated_at',
                    ],
                ]
            )
            ->assertJsonFragment(['uuid' => $company->uuid, 'name' => $companyNewName])
        ;
    }

    public function test_update_company_with_invalid_name(): void
    {
        $payload = ['name' => $this->faker->name,];
        $company = Company::create($payload);

        $this->put(sprintf('/api/v1/companies/%s', $company->uuid), $payload)
            ->assertStatus(Response::HTTP_FOUND)
            ->assertInvalid(['name'])
        ;
    }
}
